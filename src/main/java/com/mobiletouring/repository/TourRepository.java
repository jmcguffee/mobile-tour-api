package com.mobiletouring.repository;

import com.mobiletouring.domain.Owner;
import com.mobiletouring.domain.Tour;
import io.micrometer.core.lang.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

@Repository
@Transactional
public interface TourRepository extends JpaRepository<Tour,Long> {
    Optional<Tour> findByTitle(String title);
    Set<Tour> findAllByTitle(String title);
    Set<Tour> findAllByOwner(Owner owner);
    void deleteById(@NonNull Long tourId);
}
