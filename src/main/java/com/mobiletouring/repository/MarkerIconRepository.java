package com.mobiletouring.repository;

import com.mobiletouring.domain.MarkerIcon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;


@NoRepositoryBean
public interface MarkerIconRepository<T extends MarkerIcon> extends JpaRepository<T, Long> {

}
