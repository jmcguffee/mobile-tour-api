package com.mobiletouring.repository;

import com.mobiletouring.domain.CustomMarkerIcon;

import javax.transaction.Transactional;

@Transactional
public interface CustomMarkerIconRepository extends MarkerIconRepository<CustomMarkerIcon> {
}
