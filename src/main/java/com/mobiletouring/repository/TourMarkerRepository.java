package com.mobiletouring.repository;

import com.mobiletouring.domain.Tour;
import com.mobiletouring.domain.TourMarker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TourMarkerRepository extends JpaRepository<TourMarker,Long>{
    List<TourMarker> findAllByTour(Tour tour);
}
