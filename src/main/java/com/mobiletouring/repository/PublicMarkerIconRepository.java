package com.mobiletouring.repository;

import com.mobiletouring.domain.PublicMarkerIcon;

import javax.transaction.Transactional;

@Transactional
public interface PublicMarkerIconRepository extends MarkerIconRepository<PublicMarkerIcon> {
}
