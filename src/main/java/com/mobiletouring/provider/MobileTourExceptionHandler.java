package com.mobiletouring.provider;

import com.mobiletouring.domain.ExceptionResponse;
import com.mobiletouring.exception.MobileTourException;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.List;

@Provider
public class MobileTourExceptionHandler implements ExceptionMapper<MobileTourException> {

    @Context
    private HttpHeaders headers;

    @Override
    public Response toResponse(MobileTourException e) {
        return Response.status(404).entity(new ExceptionResponse(e.getMessage(), e.getErrorCode())).type( getAcceptType()).build();

    }

    private String getAcceptType(){
        List<MediaType> accepts = headers.getAcceptableMediaTypes();
        if (accepts!=null && accepts.size() > 0) {
            return accepts.get(0).toString();
        }else {
            return MediaType.APPLICATION_JSON;
        }
    }
}
