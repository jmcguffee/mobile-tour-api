package com.mobiletouring.provider;

import org.springframework.stereotype.Component;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Component
@Provider
public class CorsFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext,
                       ContainerResponseContext responseContext) throws IOException {
        if(!responseContext.getHeaders().containsKey("Access-Control-Allow-Origin")){
            responseContext.getHeaders().add(
                    "Access-Control-Allow-Origin", "http://localhost");
        }
        if(!responseContext.getHeaders().containsKey("Access-Control-Allow-Credentials")){
            responseContext.getHeaders().add(
                    "Access-Control-Allow-Credentials", true);
        }
        if(!responseContext.getHeaders().containsKey("Access-Control-Allow-Headers")){
            responseContext.getHeaders().add(
                    "Access-Control-Allow-Headers",
                    "origin, content-type, accept, authorization");
        }
        if(!responseContext.getHeaders().containsKey("Access-Control-Allow-Methods")){
            responseContext.getHeaders().add(
                    "Access-Control-Allow-Methods",
                    "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        }
    }
}
