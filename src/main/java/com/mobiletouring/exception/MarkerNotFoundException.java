package com.mobiletouring.exception;

public class MarkerNotFoundException extends MobileTourException {
    public MarkerNotFoundException(){
        super("MT-MARKER-1", "Could not find the resource you were lookking for");
    }
}
