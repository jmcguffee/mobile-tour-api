package com.mobiletouring.exception;

public class OwnerNotFoundException extends UserNotFoundException {
    public OwnerNotFoundException(){
        super("MT-OWNER-1","Could not locate the owner you were referencing");
    }
}
