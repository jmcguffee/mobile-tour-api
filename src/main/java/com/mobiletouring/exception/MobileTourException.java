package com.mobiletouring.exception;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public abstract class MobileTourException extends Exception{
    private String errorCode;

    public MobileTourException(){
        super("Generic Message");
        this.errorCode = "ABC-123";
    }

    public MobileTourException(String errorCode, String message){
        super(message);
        this.errorCode = errorCode;

    }
}
