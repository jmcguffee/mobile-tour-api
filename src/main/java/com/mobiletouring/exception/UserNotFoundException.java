package com.mobiletouring.exception;

public class UserNotFoundException extends MobileTourException {

    public UserNotFoundException(){
        super("MT-USER-1","Could not locate the user you were referencing");
    }

    public UserNotFoundException(String errorCode, String message){
        super(errorCode,message);
    }
}
