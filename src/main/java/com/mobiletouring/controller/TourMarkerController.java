package com.mobiletouring.controller;

import com.mobiletouring.domain.TourMarker;
import com.mobiletouring.exception.TourNotFoundException;
import com.mobiletouring.service.IMarkerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/v1/marker")
@Api(value = "TourMarker Controller")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Controller
public class TourMarkerController {

    private final IMarkerService markerService;


    @Autowired
    public TourMarkerController(IMarkerService markerService) {
        this.markerService = markerService;
    }

    @GET
    @Path("/{tourId}")
    @ApiOperation("Returns a list of tourMarkers associated with a Tour")
    public List<TourMarker> getMarkers(@PathParam("tourId") Long tourId) throws TourNotFoundException{
        return markerService.getAllMarkersForTour(tourId);
    }

    @POST
    @Path("/")
    @ApiOperation("Add a new TourMarker")
    public TourMarker addMarker(TourMarker tourMarker){
        return markerService.add(tourMarker);
    }

    @PUT
    @Path("/")
    @ApiOperation("Update a TourMarker")
    public TourMarker updateMarker(TourMarker tourMarker){
        return markerService.update(tourMarker);
    }

    @DELETE
    @Path("/")
    @ApiOperation("Delete a marker")
    public void deleteMarker(Long markerId){
        markerService.delete(markerId);
    }


}
