package com.mobiletouring.controller;

import com.mobiletouring.annotation.AdminOnly;
import com.mobiletouring.domain.Owner;
import com.mobiletouring.domain.User;
import com.mobiletouring.exception.UserNotFoundException;
import com.mobiletouring.service.IUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@RestController
@Path("/v1/user")
@Api("User Controller")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserController {

    @Autowired
    private IUserService userService;

    @GET
    @Path("/")
    @AdminOnly
    public Set<User> getAllUsers(){
        return userService.findAll();
    }

    @GET
    @Path("/{userId}")
    public User getUserById(@PathParam("userId") Long userId) throws UserNotFoundException {
        return userService.findById(userId);
    }

    @GET
    @Path("/{username}/{password}")
    public User getUserByUsernameAndPass(@PathParam("username") String username, @PathParam("password") String password) throws UserNotFoundException{
        return userService.getUserByUsernameAndPass(username,password);
    }

    @POST
    @Path("/")
    public User addNewUser(User user){
        return userService.add(user);
    }

    @PUT
    @Path("/")
    public User updateUser(User user){
        return userService.update(user);
    }


    @DELETE
    @Path("/{userId}")
    public void deleteUser(@PathParam("userId") Long userId){
        userService.delete(userId);
    }

    @GET
    @Path("/owner")
    @AdminOnly
    public Set<Owner> getAllOwners(){
        return userService.getAllOwners();
    }

    @GET
    @Path("/owner/{ownerId}/users")

    public Set<User> getAllUsersByOwner(@PathParam("ownerId") Long ownerId) throws Exception{
        return userService.getAllUsersForOwner(ownerId);
    }

}
