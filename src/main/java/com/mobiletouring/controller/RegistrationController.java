package com.mobiletouring.controller;

import com.mobiletouring.domain.User;
import com.mobiletouring.service.IUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/v1/register")
@Controller
@Api("Registration Controller")
public class RegistrationController {

    private final IUserService userService;

    @Autowired
    public RegistrationController(IUserService userService){
        this.userService = userService;
    }

    @POST
    @Path("/")
    public User register(User user){
        return userService.add(user);
    }
}
