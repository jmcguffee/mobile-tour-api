package com.mobiletouring.controller;

import com.mobiletouring.domain.ExceptionResponse;
import com.mobiletouring.domain.Tour;
import com.mobiletouring.exception.OwnerNotFoundException;
import com.mobiletouring.exception.TourNotFoundException;
import com.mobiletouring.service.ITourService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/v1/tour")
@Api(value = "Tour Controller")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Controller
public class TourController {

    private final ITourService tourService;

    @Autowired
    public TourController(ITourService tourService) {
        this.tourService = tourService;
    }

    @GET
    @Path("/")
    @ApiOperation("Returns a list of all the tours in the system")
    public Set<Tour> getAllTours(){
        return tourService.findAll();
    }

    @GET
    @Path("/owner/{ownerId}")
    @ApiOperation("Returns a list of all the tours that is owned by the current owner")
    @ApiResponses(
        {
            @ApiResponse(code = 200, message = "Successfully found the tours", response = Tour.class, responseContainer = "Set"),
            @ApiResponse(code = 404, message = "Could not find the resource", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "There was an internal error. A notification has been sent to our internal teams", response = ExceptionResponse.class)
        }
    )
    public Set<Tour> getAllToursByOwner(@PathParam("ownerId") Long ownerId) throws OwnerNotFoundException {
        return tourService.getAllToursForOwner(ownerId);
    }

    @GET
    @Path("/{tourId}")
    @ApiOperation("Returns a tour by resource ID")
    @ApiResponses(
        {
            @ApiResponse(code = 200, message = "Successfully found the tours", response = Tour.class),
            @ApiResponse(code = 404, message = "Could not find the resource", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "There was an internal error. A notification has been sent to our internal teams", response = ExceptionResponse.class)
        }
    )
    public Tour getTourById(@PathParam("tourId") Long tourId) throws TourNotFoundException {
        Tour tour = tourService.findById(tourId);
        if(tour == null){
            throw new TourNotFoundException();
        }
        return tour;
    }

    @POST
    @Path("/")
    public Tour createTour(Tour tour){
        return tourService.add(tour);
    }

    @PUT
    @Path("/")
    public Tour updateTour(Tour tour) {
        return tourService.update(tour);
    }

    @DELETE
    @Path("/{tourId}")
    public void deleteTour(@PathParam("tourId") Long tourId){
        tourService.delete(tourId);
    }
}
