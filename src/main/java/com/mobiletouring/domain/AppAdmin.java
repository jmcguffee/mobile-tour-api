package com.mobiletouring.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AppAdmin extends User {

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "owner_admin",
            joinColumns = { @JoinColumn(name = "admin_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    private Set<Owner> owners;

    public AppAdmin(Long id, @NotNull @Size(min = 3, max = 40) String username, @NotNull @Size(min = 8, max = 72) String password, boolean enabled, @Email String email, Set<Role> roles, Set<Owner> owners) {
        super(id, username, password, enabled, email, null, roles,true,true,true);
        this.owners = owners;
    }
}
