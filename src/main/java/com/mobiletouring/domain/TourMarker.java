package com.mobiletouring.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "tour_markers")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TourMarker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne
    @JsonIgnore
    private Tour tour;

    @NotNull
    @Size(min = 5, max = 100)
    private String title;

    @Size(max = 3000)
    private String description;

    @NotNull
    private Double lat;

    @NotNull
    private Double lng;


    @Embedded
    private MarkerRadius circle = MarkerRadius.builder().build();

    @ManyToOne
    @NotNull
    private MarkerIcon icon;

    private String audio;

    private String video;

    private int sortOrder;

    private Boolean draggable = true;

}
