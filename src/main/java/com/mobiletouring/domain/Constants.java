package com.mobiletouring.domain;

public class Constants {
    public static final String APP_ADMIN = "ROLE_APP_ADMIN";
    public static final String ADMIN = "ROLE_ADMIN";
    public static final String SUPER_USER = "ROLE_SUPER_USER";
    public static final String STANDARD_USER = "ROLE_STANDARD_USER";
}
