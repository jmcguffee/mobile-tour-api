package com.mobiletouring.domain;

import lombok.*;

import javax.persistence.Embeddable;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class MarkerIconScale {
    @Builder.Default
    private int height = 40;
    @Builder.Default
    private int width = 40;
}
