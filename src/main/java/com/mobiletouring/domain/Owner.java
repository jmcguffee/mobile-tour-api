package com.mobiletouring.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Owner extends User {


    @Builder
    public Owner(Long id, @NotNull @Size(min = 3, max = 20) String username, @NotNull @Size(min = 8, max = 25) String password, boolean enabled, String email, Owner owner, String businessName, HashSet<Role> roles) {
        super(id, username, password, enabled, email, owner, roles,true,true,true);
        this.businessName = businessName;
    }

    private String businessName;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id")
    private Set<Contact> contacts;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id")
    private Set<Tour> tours;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id")
    private Set<CustomMarkerIcon> customMarkers;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id")
    private Set<User> users;

    @ManyToMany(mappedBy = "owners", cascade = CascadeType.ALL)
    private Set<AppAdmin> admins;

}
