package com.mobiletouring.domain;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomPrincipal {
    private String username;
    private String email;
}
