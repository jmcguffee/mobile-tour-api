package com.mobiletouring.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@Entity
@Table(name = "tours")
public class Tour {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(targetEntity = Owner.class)
    @JsonIgnore
    private Owner owner;

    @Size(min = 4, max = 200)
    private String title;

    @Size(max = 3000)
    private String description;

    private String introAudio;

    private String introVideo;

    private double startLat;

    private double startLng;

    private String iconSmall;

    private String iconLarge;

    private String backgroundImage;

    private Boolean autoPlay = true;

    private Boolean autoZoom = false;

    @OneToMany(mappedBy = "tour", fetch = FetchType.EAGER)
    private Set<TourMarker> tourMarkers = new HashSet<>();

}
