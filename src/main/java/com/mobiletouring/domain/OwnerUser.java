package com.mobiletouring.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class OwnerUser extends User{

    private String email;

    public OwnerUser(Long id, @NotNull @Size(min = 3, max = 40) String username, @NotNull @Size(min = 8, max = 72) String password,boolean enabled, String email, Owner owner, HashSet<Role> roles) {
        super(id, username, password, enabled, email, owner, roles,true,true,true);
    }
}
