package com.mobiletouring.domain;

import lombok.*;

import javax.persistence.Embeddable;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class MarkerRadius {
    @Builder.Default
    private int radius = 20;
    @Builder.Default
    private String fillColor = "#7FFFD4";
    @Builder.Default
    private double opacity = .45;
}
