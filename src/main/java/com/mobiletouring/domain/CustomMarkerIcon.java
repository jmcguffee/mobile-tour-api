package com.mobiletouring.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class CustomMarkerIcon extends MarkerIcon {

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, targetEntity = Owner.class)
    @JsonIgnore
    private Owner owner;

    @Builder
    public CustomMarkerIcon(@NotNull String url, @NotNull MarkerIconScale scaledSize, @NotNull Owner owner) {
        super(null, url, scaledSize);
        this.owner = owner;
    }
}
