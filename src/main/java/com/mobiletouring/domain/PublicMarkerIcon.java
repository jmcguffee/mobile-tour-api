package com.mobiletouring.domain;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class PublicMarkerIcon extends MarkerIcon{

    @Builder
    private PublicMarkerIcon(){
        super(null,"../../assets/GMapMarker.png", MarkerIconScale.builder().build());
    }

}
