package com.mobiletouring.domain;

public enum RoleEnum {
    ROLE_STANDARD_USER("ROLE_STANDARD_USER"),
    ROLE_SUPER_USER("ROLE_SUPER_USER"),
    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_APP_ADMIN("ROLE_APP_ADMIN");

    private String role;

    RoleEnum(String value){
        this.role = value;
    }

    public boolean equals(String role){
        return this.role.equals(role);
    }

    public String toString(){
        return this.name();
    }
}
