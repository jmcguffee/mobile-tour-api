package com.mobiletouring.service;

import com.mobiletouring.domain.TourMarker;
import com.mobiletouring.exception.MarkerNotFoundException;
import com.mobiletouring.exception.TourNotFoundException;

import java.util.List;

public interface IMarkerService extends ICrudService<TourMarker,Long, MarkerNotFoundException>{

    List<TourMarker> getAllMarkersForTour(Long tourId) throws TourNotFoundException;
}
