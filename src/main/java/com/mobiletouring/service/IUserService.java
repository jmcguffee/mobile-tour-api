package com.mobiletouring.service;

import com.mobiletouring.domain.Owner;
import com.mobiletouring.domain.User;
import com.mobiletouring.exception.OwnerNotFoundException;
import com.mobiletouring.exception.UserNotFoundException;

import java.util.Set;

public interface IUserService extends ICrudService<User,Long, UserNotFoundException>{

    Set<User> getAllUsersForOwner(Long ownerId) throws OwnerNotFoundException;
    Set<Owner> getAllOwners();
    User getUserByUsernameAndPass(String username, String password) throws UserNotFoundException;
}
