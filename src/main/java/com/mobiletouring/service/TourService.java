package com.mobiletouring.service;

import com.mobiletouring.domain.Owner;
import com.mobiletouring.domain.Tour;
import com.mobiletouring.domain.User;
import com.mobiletouring.exception.OwnerNotFoundException;
import com.mobiletouring.exception.TourNotFoundException;
import com.mobiletouring.exception.UserNotFoundException;
import com.mobiletouring.repository.TourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Component
public class TourService implements ITourService{

    private final TourRepository tourRepository;

    private final UserService userService;

    @Autowired
    public TourService(TourRepository tourRepository, UserService userService) {
        this.tourRepository = tourRepository;
        this.userService = userService;
    }


    @Override
    @Transactional
    public Set<Tour> getAllToursForOwner(Long ownerId) throws OwnerNotFoundException {
        User user;
        try {
            user = userService.findById(ownerId);
        }
        catch (UserNotFoundException e){
            throw new OwnerNotFoundException();
        }
        return tourRepository.findAllByOwner((Owner) user);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public Set<Tour> findAll() {
        return new HashSet<>(tourRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public Tour findById(Long id) throws TourNotFoundException {
        return tourRepository.findById(id).orElseThrow(TourNotFoundException::new);
    }

    @Override
    public Tour add(Tour item) {
        return tourRepository.saveAndFlush(item);
    }

    @Override
    public Tour update(Tour item) {
        return tourRepository.saveAndFlush(item);
    }

    @Override
    public void delete(Long id) {
        tourRepository.deleteById(id);
    }
}
