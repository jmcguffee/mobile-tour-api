package com.mobiletouring.service;

import com.mobiletouring.exception.MobileTourException;

import java.util.Set;

public interface ICrudService<T,K,J extends MobileTourException> {

    Set<T> findAll();
    T findById(K id) throws J;
    T add(T item);
    T update(T item);
    void delete(K id);
}
