package com.mobiletouring.service;

import com.mobiletouring.domain.Tour;
import com.mobiletouring.exception.OwnerNotFoundException;
import com.mobiletouring.exception.TourNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public interface ITourService extends ICrudService<Tour,Long,TourNotFoundException> {
    Set<Tour> getAllToursForOwner(Long ownerId) throws OwnerNotFoundException;
}
