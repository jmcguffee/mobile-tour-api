package com.mobiletouring.service;

import com.mobiletouring.domain.Tour;
import com.mobiletouring.domain.TourMarker;
import com.mobiletouring.exception.MarkerNotFoundException;
import com.mobiletouring.exception.TourNotFoundException;
import com.mobiletouring.repository.TourMarkerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class MarkerService implements IMarkerService {

    private final TourMarkerRepository tourMarkerRepository;

    private final ITourService tourService;

    @Autowired
    public MarkerService(TourMarkerRepository tourMarkerRepository, ITourService tourService) {
        this.tourMarkerRepository = tourMarkerRepository;
        this.tourService = tourService;
    }

    public List<TourMarker> getAllMarkersForTour(Long tourId) throws TourNotFoundException{
        Tour tour = tourService.findById(tourId);
        if(tour == null){
            throw new TourNotFoundException();
        }
        return tourMarkerRepository.findAllByTour(tour);
    }

    @Override
    public Set<TourMarker> findAll() {
        return new HashSet<>(tourMarkerRepository.findAll());
    }

    @Override
    public TourMarker findById(Long id) throws MarkerNotFoundException {
        return tourMarkerRepository.findById(id).orElseThrow(MarkerNotFoundException::new);
    }

    @Override
    public TourMarker add(TourMarker item) {
        return null;
    }

    @Override
    public TourMarker update(TourMarker item) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
