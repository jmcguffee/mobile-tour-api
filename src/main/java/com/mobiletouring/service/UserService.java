package com.mobiletouring.service;

import com.mobiletouring.domain.Owner;
import com.mobiletouring.domain.User;
import com.mobiletouring.exception.OwnerNotFoundException;
import com.mobiletouring.exception.UserNotFoundException;
import com.mobiletouring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Component
public class UserService implements IUserService{

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public Set<User> getAllUsersForOwner(Long ownerId) throws OwnerNotFoundException {
        User owner;
        try{
            owner = findById(ownerId);
        }
        catch(UserNotFoundException e){
            throw new OwnerNotFoundException();
        }

        return new HashSet<>(userRepository.findAllByOwnerId(owner.getId()));
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public User getUserByUsernameAndPass(String username, String password) throws UserNotFoundException{
        User user = userRepository.findByUsername(username).orElseThrow(UserNotFoundException::new);
        boolean matches = passwordEncoder.matches(password,user.getPassword());
        if(matches){
            return user;
        }
        throw new UserNotFoundException();
    }


    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public Set<Owner> getAllOwners(){
        return new HashSet<>(userRepository.findAllByOwnerIsNull());
    }


    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public Set<User> findAll() {
        return new HashSet<>(userRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public User findById(Long id) throws UserNotFoundException {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    @Override
    @Transactional
    public User add(User item) {
        return userRepository.saveAndFlush(item);
    }

    @Override
    @Transactional
    public User update(User item) {
        return userRepository.saveAndFlush(item);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        userRepository.deleteById(id);
    }
}
