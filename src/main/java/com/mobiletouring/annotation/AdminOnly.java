package com.mobiletouring.annotation;

import org.springframework.security.access.annotation.Secured;

import java.lang.annotation.*;

import static com.mobiletouring.domain.Constants.ADMIN;
import static com.mobiletouring.domain.Constants.APP_ADMIN;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Secured({ADMIN,APP_ADMIN})
public @interface AdminOnly {
}
