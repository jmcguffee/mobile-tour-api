package com.mobiletouring.annotation;

import org.springframework.security.access.annotation.Secured;

import java.lang.annotation.*;

import static com.mobiletouring.domain.Constants.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Secured({APP_ADMIN,ADMIN,SUPER_USER,STANDARD_USER})
public @interface UserAccess {
}
