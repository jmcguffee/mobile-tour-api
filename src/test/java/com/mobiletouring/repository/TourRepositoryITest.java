package com.mobiletouring.repository;

import com.mobiletouring.domain.Owner;
import com.mobiletouring.domain.Role;
import com.mobiletouring.domain.RoleEnum;
import com.mobiletouring.domain.Tour;
import com.mobiletouring.exception.TourNotFoundException;
import com.mobiletouring.model.BaseIntegrationTest;
import com.mobiletouring.setup.SetupHelper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@DataJpaTest
public class TourRepositoryITest extends BaseIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TourRepository tourRepository;

    @Before
    public void setUp(){
        Role adminRole = Role.builder().role(RoleEnum.ROLE_ADMIN).build();
        Role userRole = Role.builder().role(RoleEnum.ROLE_STANDARD_USER).build();

        entityManager.persist(adminRole);
        entityManager.persist(userRole);

        Set<Role> roles = new HashSet<>(Arrays.asList(adminRole,userRole));

        Owner owner = SetupHelper.getOwner();
        owner.setRoles(roles);
        Tour tour = SetupHelper.getTour();


        assertNull(tour.getId());

        entityManager.persist(owner);

        tour.setOwner(owner);
        entityManager.persist(tour);
        entityManager.flush();

        assertNotNull(tour.getId());


    }

    @Test
    public void findToursByTitleTest(){
        Set<Tour> tours = tourRepository.findAllByTitle(SetupHelper.TOUR_TITLE);

        assertEquals(tours.size(),1);
    }

    @Test
    public void findTourByTitleTest() throws TourNotFoundException{
        Tour tour = tourRepository.findByTitle(SetupHelper.TOUR_TITLE).orElseThrow(TourNotFoundException::new);
        assertEquals(tour.getTitle(),SetupHelper.TOUR_TITLE);

    }

}
