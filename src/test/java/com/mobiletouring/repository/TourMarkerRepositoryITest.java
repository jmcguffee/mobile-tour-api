package com.mobiletouring.repository;

import com.mobiletouring.domain.MarkerIcon;
import com.mobiletouring.domain.PublicMarkerIcon;
import com.mobiletouring.domain.Tour;
import com.mobiletouring.domain.TourMarker;
import com.mobiletouring.model.BaseIntegrationTest;
import com.mobiletouring.setup.SetupHelper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.junit.Assert.*;

@DataJpaTest
public class TourMarkerRepositoryITest extends BaseIntegrationTest {


    @Autowired
    private TourMarkerRepository tourMarkerRepository;

    @Autowired
    private TestEntityManager entityManager;

    private Tour tour;

    @Before
    public void setUp(){

        tour = SetupHelper.getTour();
        entityManager.persist(tour);

        Tour tour2 = SetupHelper.getTour();
        entityManager.persist(tour2);

        MarkerIcon markerIcon = PublicMarkerIcon.builder().build();
        entityManager.persist(markerIcon);

        TourMarker tourMarker1 = SetupHelper.getPublicMarker("Test TourMarker");
        tourMarker1.setTour(tour);
        tourMarker1.setIcon(markerIcon);

        assertNull(tourMarker1.getId());

        entityManager.persist(tourMarker1);

        assertNotNull(tourMarker1.getId());

        TourMarker tourMarker2 = SetupHelper.getPublicMarker("Test TourMarker - 2");
        tourMarker2.setTour(tour);
        tourMarker2.setIcon(markerIcon);

        assertNull(tourMarker2.getId());

        entityManager.persist(tourMarker2);

        assertNotNull(tourMarker2.getId());

        TourMarker tourMarker3 = SetupHelper.getPublicMarker("Test TourMarker - 3");
        tourMarker3.setTour(tour2);
        tourMarker3.setIcon(markerIcon);

        entityManager.persist(tourMarker3);
        entityManager.flush();
    }


    @Test
    public void findAllByTour(){
        List<TourMarker> myTourMarkers = tourMarkerRepository.findAllByTour(tour);
        assertEquals(2, myTourMarkers.size());

    }

}
