package com.mobiletouring.setup;

import com.mobiletouring.domain.*;

import java.util.HashSet;
import java.util.Set;

public class SetupHelper {

    public static final String TOUR_TITLE = "hello";

    public static Tour getTour(){
        return Tour.builder()
                .title(TOUR_TITLE)
                .description("Test Tour")
                .autoPlay(false)
                .autoZoom(false)
                .owner(null)
                .build();
    }


    public static TourMarker getPublicMarker(String title){
        return TourMarker.builder()
                .title(title)
                .lat(30.2672)
                .lng(-97.7431)
                .description("A test marker")
                .circle(MarkerRadius.builder().build())
                .icon(PublicMarkerIcon.builder().build())
                .build();
    }

    public static TourMarker getCustomMarker(String title, Owner owner){
        return TourMarker.builder()
                .title(title)
                .lat(30.2672)
                .lng(-97.7431)
                .description("A test marker")
                .circle(MarkerRadius.builder().build())
                .icon(PublicMarkerIcon.builder().build())
                .build();
    }

    public static Set<Tour> getAllToursHelper(){
        Set<Tour> tours = new HashSet<>();
        tours.add(getTour());
        Tour tour2 = getTour();
        tour2.setTitle(TOUR_TITLE + " 2");
        tours.add(tour2);

        return tours;
    }

    public static Owner getOwner(){
        return Owner.builder()
                .businessName("Some business")
                .username("test")
                .password("testtest")
                .build();
    }



}
