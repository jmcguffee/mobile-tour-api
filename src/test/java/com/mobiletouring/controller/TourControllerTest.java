package com.mobiletouring.controller;

import com.mobiletouring.domain.Tour;
import com.mobiletouring.exception.OwnerNotFoundException;
import com.mobiletouring.exception.TourNotFoundException;
import com.mobiletouring.model.BaseUnitTest;
import com.mobiletouring.service.ITourService;
import com.mobiletouring.setup.SetupHelper;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class TourControllerTest extends BaseUnitTest {

    @InjectMocks
    private TourController tourController;

    @Mock
    private ITourService tourService;


    @Test
    public void getToursTest(){
        when(tourService.findAll()).thenReturn(SetupHelper.getAllToursHelper());
        assertEquals(tourController.getAllTours().size(),2);
    }

    @Test
    public void getToursByOwnerTest() throws OwnerNotFoundException {
        when(tourService.getAllToursForOwner(anyLong())).thenReturn(SetupHelper.getAllToursHelper());
        assertEquals(tourController.getAllToursByOwner(1L).size(),1);
    }

    @Test(expected = OwnerNotFoundException.class)
    public void getToursByOwnerTest_Exception() throws OwnerNotFoundException {
        when(tourService.getAllToursForOwner(anyLong())).thenThrow(new OwnerNotFoundException());
        tourController.getAllToursByOwner(1L);
    }

    @Test
    public void getTourByIdTest() throws TourNotFoundException{
        when(tourService.findById(anyLong())).thenReturn(SetupHelper.getTour());
        Tour tour = tourController.getTourById(1L);
        assertNotNull(tour);
        assertEquals(tour.getTitle(),SetupHelper.TOUR_TITLE);
    }

    @Test(expected = TourNotFoundException.class)
    public void getTourByIdTest_Exception() throws TourNotFoundException{
        when(tourService.findById(anyLong())).thenThrow(new TourNotFoundException());
        Tour tour = tourController.getTourById(1L);
    }

    @Test
    public void createTourTest(){
        when(tourService.add(any(Tour.class))).thenReturn(SetupHelper.getTour());
        Tour tour = tourController.createTour(new Tour());
        assertNotNull(tour);
        assertEquals(tour.getTitle(),SetupHelper.TOUR_TITLE);
    }

    @Test
    public void updateTourTest(){
        when(tourService.update(any(Tour.class))).thenReturn(SetupHelper.getTour());
        Tour tour = tourController.createTour(new Tour());
        assertNotNull(tour);
        assertEquals(tour.getTitle(),SetupHelper.TOUR_TITLE);
    }

    @Test
    public void deleteTourTest(){
        doNothing().when(tourService).delete(anyLong());
    }

}
